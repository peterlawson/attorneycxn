from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.timezone import now

from django.http import FileResponse, HttpResponse

from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

import csv


from main.forms import UploadLegalDoc, ClientForm, AddAttorneyForm, CustomUserCreationForm, CaseForm
from main.models import Attorney, Client, Case, LegalDoc, User

from main.utilities import (
  assign_attorney,
  email_form_to_attorney,
  gen_case_id,
  attorney_list_upload_handler,
  temp_password,
)



class RegisterUserView(FormView):
  template_name = 'registration/register.html'
  form_class = CustomUserCreationForm
  success_url = '/success'

  def get(self, request, *args, **kwargs):
    form = self.form_class()
    return render(request, self.template_name, {'form': form})

  def form_valid(self, form, *args, **kwargs):
    # This method is called when valid form data has been POSTed.
    # It should return an HttpResponse.
    user_data = form.cleaned_data
    new_user = User.objects.create_user(
      username=user_data['username'],
      email=user_data['email'],
      password=user_data['password1']
    )
    current_user = authenticate(self.request, username=new_user.username, password=new_user.password)
    if current_user is not None:
      login(request, current_user)
    return super().form_valid(form)



@login_required
def upload_form(request):
  if request.method == 'POST':
    client_form = ClientForm(request.POST)
    legal_form = UploadLegalDoc(request.POST, request.FILES)
    case_form = CaseForm(request.POST)

    if legal_form.is_valid():
      # Does this catch errors?
      pass

    u = request.user

    try:
      client = Client.objects.get(user=u)
    except Client.DoesNotExist:
      # If no Client associated with user (i.e. this is their first form submission),
      # then create one (if form is valid)
      if client_form.is_valid():
        client_form_data = client_form.cleaned_data
        # save Client to db if new
        client = Client.objects.create(
          user=u,
          name=client_form_data['name'],
          county=client_form_data['county'],
          state=client_form_data['state'],
          email=client_form_data['email'],
          phone=client_form_data['phone']
        )

    # get attorney to set on Case object
    case_attorney = assign_attorney(client.state, client.county, request.POST['category'])

    # create Case
    new_case = Case.objects.create(
      client=client,
      attorney=case_attorney,
      ## Should make sure case_id doesn't already exist in db
      case_id=gen_case_id(),
      category=request.POST['category'],
    )

    # save LegalDoc to db
    new_legal_doc = LegalDoc.objects.create(
      doc_title=request.POST['doc_title'],
      pdf_file=request.FILES['pdf_file'],
      client=client,
      case=new_case
    )

    # to include in email to attorney
    client_info = {
      'name': client.name,
      'email': client.email,
      'phone': client.phone
    }

    # email form and client info to attorney
    email_to_attorney = email_form_to_attorney(case_attorney.email, client_info, new_legal_doc)

    ## may want to set num_days somewhere else
    num_days = 2
    return render(
      request,
      'upload_successful.html',
      {'num_days': num_days, 'case_id': new_case.case_id}
    )
  else:
    client_form = ClientForm()
    legal_form = UploadLegalDoc()
    case_form = CaseForm()
  return render(request, 'upload_form.html', {'client_form': client_form, 'legal_form': legal_form, 'case_form': case_form})

def view_document(request, doc_uuid):
  d = LegalDoc.objects.get(doc_uuid=doc_uuid)
  pdf_file_name = d.pdf_file.name
  file_data = FileResponse(open(pdf_file_name, 'rb'))
  response = HttpResponse(file_data, content_type='application/pdf')
  return response

@login_required
def case_lookup(request):
  if request.method == 'POST':
    # Use `reverse` here?
    return redirect('/case/{0}'.format(request.POST['case-id']))
  else:
    return render(request, 'case_lookup.html')

@login_required
def case_details(request, case_id):
  '''
    TODO: check whether user is Client or Attorney
    and show different view to each.
  '''
  if request.method == 'GET':
    u = request.user
    case = get_object_or_404(Case, case_id=case_id)
    # Make sure Client owns this Case
    # try:
    #   # client = get_object_or_404(Client, user=u)
    #   client = Client.objects.get(user=u)
    # except Client.DoesNotExist:
    #   # attorney = get_object_or_404(Attorney, user=u)
    #   attorney = Attorney.objects.get(user=u)
    # except Attorney.DoesNotExist:
    #   raise Http404()

    # This check assumes we aren't using the try/except cases above
    if hasattr(u, 'client'):
      client = Client.objects.get(user=u)
      if case.client == client:
        # This assumes that Attorney has been assigned. In the future, it may not happen immediately;
        # e.g., Attorney may have to accept Case.
        if (now().date() - case.opened_on).days > 3 or not case.is_open:
          attorney_email = case.attorney.email
        else:
          attorney_email = None

        case_docs = LegalDoc.objects.filter(case=case)
        title_and_remarks = [(doc.doc_title, doc.client_remarks) for doc in case_docs]

        return render(
          request,
          'case_client.html',
          {
            'opened_on': case.opened_on,
            'attorney_name': case.attorney.name,
            'title_and_remarks': title_and_remarks,
            'case_id': case.case_id,
            'is_open': case.is_open,
            'closed_on': case.closed_on,
            'attorney_email': attorney_email,
            'client_confirmed_closed': case.client_confirmed_closed
          }
        )
    elif hasattr(u, 'attorney'):
      attorney = Attorney.objects.get(user=u)
      case_docs = LegalDoc.objects.filter(case=case)
      title_and_remarks = [(doc.doc_title, doc.client_remarks) for doc in case_docs]

      return render(
        request,
        'case_attorney.html',
        {
          'opened_on': case.opened_on,
          'client_name': case.client.name,
          'title_and_remarks': title_and_remarks,
          'case_id': case.case_id,
          'is_open': case.is_open,
          'closed_on': case.closed_on,
          'client_email': case.client.email,
          'attorney_confirmed_closed': case.attorney_confirmed_closed
        }
      )
    else:
      return HttpResponseNotFound()
  else:
    url = reverse('case-lookup')
    return redirect(url)

@login_required
def close_case(request, case_id):
  if request.method == 'POST':
    u = request.user
    ## Need to check whether user is Attorney or Client
    if hasattr(u, 'client'):
      client = Client.objects.get(user=u)
      case = get_object_or_404(Case, case_id=case_id, client=client)
      # if user is client:
      case.client_confirmed_closed = True
      # else, set attorney_confirmed_closed to True

      # if attorney (i.e. other party) has also marked 'resolved', then close case
      if case.attorney_confirmed_closed:
        case.is_open = False
        case.closed_on = now().date()
      case.save()

    elif hasattr(u, 'attorney'):
      attorney = Attorney.objects.get(user=u)
      case = get_object_or_404(Case, case_id=case_id, attorney=attorney)
      case.attorney_confirmed_closed = True
      if case.client_confirmed_closed:
        case.is_open = False
        case.closed_on = now().date()
      case.save()

  url = reverse('case-details', kwargs={'case_id': case_id})
  return redirect(url)

@staff_member_required
def add_attorneys(request):
  '''
    Post expects a CSV doc
  '''
  if request.method == 'POST':
    # First, save the file
    csv_file = attorney_list_upload_handler(request.FILES['attorneys_list_csv'])
    # Then read rows from CSV
    with open(csv_file, 'r') as attorneys_list:
      row_reader = csv.reader(attorneys_list)
      keys = next(row_reader)
      for row in row_reader:
        # Blank dict
        d = {}
        for i in range(len(row)):
          # Build dict with field names as keys and values from file rows
          d[str(keys[i])] = row[i]
        # Create associated User
        atty_user = User.objects.create_user(
          username=d['email'],
          email=d['email'],
          password=temp_password()
        )
        d['user'] = atty_user
        new_atty = Attorney(**d)
        # If new_atty is only in her area with the specialization (rare), make her 'next'
        siblings = Attorney.objects.filter(county=new_atty.county, state=new_atty.state, specialization=new_atty.specialization)
        if len(siblings) == 0:
          new_atty.is_next = True
        new_atty.save()
    return redirect('/admin/main/attorney')
  else:
    add_attorney_form = AddAttorneyForm()
    return render(request, 'add_attorneys.html', {'form': add_attorney_form})
