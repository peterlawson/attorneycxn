from django.core.management.base import BaseCommand

from django.conf import settings

from main.models import User

adminpw = settings.ADMIN_PW


class Command(BaseCommand):
  def handle(self, *args, **options):
    if not User.objects.filter(username="attorneycxn-admin").exists():
      User.objects.create_superuser("attorneycxn-admin", "attorneycxn@gmail.com", adminpw)
