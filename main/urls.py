from django.urls import path, include
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('upload-form', views.upload_form, name='upload-form'),
    path('faq', TemplateView.as_view(template_name='faq.html'), name='faq'),
    path('case-lookup', views.case_lookup, name='case-lookup'),
    path('case/<str:case_id>', views.case_details, name='case-details'),
    path('close-case/<str:case_id>', views.close_case, name='close-case'),
    path('case/document/<uuid:doc_uuid>', views.view_document, name='view-document'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register', views.RegisterUserView.as_view(), name='register'),
    path('success', TemplateView.as_view(template_name='registration/register_success.html'), name='register-success'),
    path('attorney-login-help', TemplateView.as_view(template_name='attorney_login_help.html'), name='attorney-login-help'),
    path('acxn-admin/add-attorneys', views.add_attorneys, name='add-attorneys'),
]

# Urls from django.contrib.auth:
# accounts/login/ [name='login']
# accounts/logout/ [name='logout']
# accounts/password_change/ [name='password_change']
# accounts/password_change/done/ [name='password_change_done']
# accounts/password_reset/ [name='password_reset']
# accounts/password_reset/done/ [name='password_reset_done']
# accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
# accounts/reset/done/ [name='password_reset_complete']
