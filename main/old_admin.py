from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Attorney, Client, Case, LegalDoc

admin.site.register(User, UserAdmin)

class AttorneyAdmin(admin.ModelAdmin):
  pass
admin.site.register(Attorney, AttorneyAdmin)

class ClientAdmin(admin.ModelAdmin):
  pass
admin.site.register(Client, ClientAdmin)

class CaseAdmin(admin.ModelAdmin):
  pass
admin.site.register(Case, CaseAdmin)

class LegalDocAdmin(admin.ModelAdmin):
  pass
admin.site.register(LegalDoc, LegalDocAdmin)


# Custom Admin site
class ACxnAdminSite(admin.AdminSite):
  site_header = 'AttorneyCxn Admin'
  site_title = 'AttorneyCxn Admin'

acxn_admin_site = ACxnAdminSite(name='acxnadmin')
acxn_admin_site.register(User)
acxn_admin_site.register(Attorney)
acxn_admin_site.register(Client)
acxn_admin_site.register(Case)
acxn_admin_site.register(LegalDoc)
