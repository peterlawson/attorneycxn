from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.conf import settings
from django.contrib.auth import get_user_model
from main.models import LegalDoc, Client, Attorney, Case

class AddAttorneyForm(forms.ModelForm):
  class Meta:
    model = Attorney
    exclude = ['is_next', 'is_active']

class ClientForm(forms.ModelForm):
  class Meta:
    model = Client
    exclude = ['user']

class UploadLegalDoc(forms.ModelForm):
  class Meta:
    model = LegalDoc
    fields = ['pdf_file', 'doc_title']

# class UploadLegalDoc(forms.Form):
#     # title = forms.CharField(max_length=50)
#     pdf_file = forms.FileField()

class CustomUserCreationForm(UserCreationForm):
  class Meta(UserCreationForm.Meta):
    model = get_user_model()
    fields = UserCreationForm.Meta.fields + ('email',)

class CaseForm(forms.ModelForm):
  class Meta:
    model = Case
    fields = ['category']
