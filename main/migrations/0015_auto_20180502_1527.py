# Generated by Django 2.0.4 on 2018-05-02 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_legaldoc_doc_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='attorney_confirmed_closed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='case',
            name='client_confirmed_closed',
            field=models.BooleanField(default=False),
        ),
    ]
