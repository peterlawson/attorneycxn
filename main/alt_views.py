def case_lookup(request):
  if request.method == 'POST':
    url = reverse('case-details', kwargs={'case_id': request.POST['case-id']})
    return redirect(url)
  else:
    return render(request, 'case_lookup.html')

@login_required
def case_details(request, case_id=None):
  '''
    It's not clear whether a Client would need the site to see Case details.
    They probably know what's going on through their correspondence
    with their Attorney.
  '''
  if request.method == 'GET':
    c = Case.objects.get(case_id=case_id)
    return render(
      request,
      'case.html',
      {
        'opened_on': c.opened_on,
        'attorney': c.attorney.name,
        'case_id': c.case_id,
        'is_open': c.is_open,
        'closed_on': c.closed_on
      }
    )
  else:
    url = reverse('case-lookup')
    return redirect(url)

# Testing links to legal docs
def doc_link_test(request, doc_uuid):
  # doc_link = 'main/legaldocs/Axiom_UG_EN_anqIZyV.pdf'
  # return render(request, 'doc_link_test.html', {'doc_link': doc_link})
  file_data = FileResponse(open('main/legaldocs/Axiom_UG_EN_anqIZyV.pdf', 'rb'))
  response = HttpResponse(file_data, content_type='application/pdf')
  # response['Content-Disposition'] = 'attachment; filename="Axiom_UG_EN_anqIZyV.pdf"'
  return render(request, 'doc_link_test.html', {'doc_link': doc_link})
