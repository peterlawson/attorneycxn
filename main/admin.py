from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from main import models as main_models

import bulk_admin # Needed for bulk adding Attorneys

# Custom Admin site
class ACxnAdminSite(admin.AdminSite):
  site_header = 'AttorneyCxn Admin'
  site_title = 'AttorneyCxn Admin'

acxn_admin_site = ACxnAdminSite(name='acxnadmin')
acxn_admin_site.register(main_models.User)
# acxn_admin_site.register(main_models.Attorney) # Skip this here and register below using BulkModelAdmin
acxn_admin_site.register(main_models.Client)
acxn_admin_site.register(main_models.Case)
acxn_admin_site.register(main_models.LegalDoc)

admin.site.register(main_models.User, UserAdmin)

@admin.register(main_models.Attorney, site=acxn_admin_site)
class AttorneyAdmin(bulk_admin.BulkModelAdmin):
  pass

@admin.register(main_models.Client)
class ClientAdmin(admin.ModelAdmin):
  pass

@admin.register(main_models.Case)
class CaseAdmin(admin.ModelAdmin):
  pass

@admin.register(main_models.LegalDoc)
class LegalDocAdmin(admin.ModelAdmin):
  pass
