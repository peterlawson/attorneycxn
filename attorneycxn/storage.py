#####
#
# This module is currently unused because package 'django-storages' was installed
# and provides this functionality.
#
#####

from django.conf import settings
from django.core.files.storage import Storage

import boto3
s3 = boto3.resource('s3')

class S3Storage(Storage):
    def __init__(self, option=None):
        if not option:
            option = settings.CUSTOM_STORAGE_OPTIONS

    def _open(self, name):
      pass

    def _save(self, name, content):
      s3.Bucket('elasticbeanstalk-us-east-2-556785711621').put_object(Key=name, Body=content)
